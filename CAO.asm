; Read the (incomplete) project description
;
;Code was compiled and tested on EMU8086 and took around 7-8 passes
;
;License: GPLv3
;Copyright: Hussain Parsaiyan
;Coding period: 8 days
;*****************Macros****************

input macro chars, display, clearscreen,userno,begin,wt ;MR. DO-IT-ALL macro :P

local disp, k1, here, clss, noclear,here1,endd,ast, upr_lim,comparing, wrong,k1_cntd, bg, bg_true  ; local variables
local master_triggered
;    pusha
;    call    master_chck ;well thought over, ended up commenting it
;    call    clrkeybuf
cmp     clss,1
jne     noclear
call    cls

noclear:

mov     input_char_counter, chars
lea     si, co_user
cmp     bg, 1
je      bg_true
jmp     k1
bg      db begin
disp    db display
clss    db clearscreen

bg_true:

mov     al,1
mov     in_input_macro,al
call    w8inp
call    master
cmp     master_trigger,1
je      origin
cmp     al, 42
jne     wrong
jmp     endd

;master_triggered:
;
;    mov     al,0
;    mov     master_trigger,al
;    popa
;    jmp     origin
;
k1:

w8      wt,1,1
dec     input_char_counter
cmp     al,0
je      wrong

comparing:

cmp     al,48
jnae    wrong

upr_lim:

cmp     al,57
jnbe    wrong

k1_cntd:

mov     [si], al
inc     si
cmp     disp,1
jne     here
mov     output, al
outt    output,0,0

here:

cmp     input_char_counter,0
jne     k1
mov     al, userno ;the performs the cogulations of 2 numbers in the array,
cmp     al, 0 ;1st bit multiplies 10 and add the 3rd cell of the array, 2nd bit added to 3rd too(ascii code -48 done to)
je      endd
mov     al,0
mov     al,co_user[0]
sub     al,48
mov     bl, 10
mul     bl
mov     co_user[0],al
mov     co_user[2],0
add     co_user[2],al
mov     cl, co_user[1]
sub     cl,48
add     co_user[2],cl
jmp     endd

wrong:

sec_triggered_check
jmp     origin

endd:

;popa

endm

;****************************

random macro    limit

;ask for 5 pass digs min. digits<=6,7<digits<=10 ask 7 digits, digits>=11 ask 9
;donot repeat the same digit number more then once
;duh should be in the range

local this1,mov_to_ran,checkin_reps,this,endd;,lim
local master_triggered
;pusha
lea             si,ran
cal_the_range   limit ; range in BH
mov         cx, 0
push        si
jmp         this

;master_triggered:
;    mov     al,0
;    mov     master_trigger,al
;    pop     si
;    jmp     origin
;
this:

call    master_chck
cmp     master_trigger,1
je      origin
pop     si
push    cx


this1:

time; time in ah
cmp     ah,limit
ja      this1
pop     cx
mov     al,0
push    si

check_reps:
cmp     cl,bh
ja      endd
cmp     al,cl
ja      mov_to_ran
cmp     [si],ah
je      this
inc     si
inc     al
jmp     check_reps

mov_to_ran:

pop     si
cmp     cl,bh
ja      endd
mov     ch,0
push    si
cmp     ah,0
je      this
add     si, cx
mov     [si],ah
inc     cl
jmp this

endd:

;popa

endm

;*****************************

time macro

local summing

mov     ah,2ch
int     21h

summing:

mov     ah, 0
mov     al, dl
mov     bl, 10
idiv    bl
add     ah, al

endm

;*****************************

cal_the_range macro lim

local this

mov     bh, 4
cmp     lim, 6
jbe     this
mov     bh, 6
cmp     lim,10
jbe     this
mov     bh,7

this:

endm

;****************************

w8 macro secs, interrupt,disp_time

local endd,this,int_enabled,int_disabled,endd_with_input,endd_with_no_input, here,input
local master_triggered, master_triggered1

;    pusha
mov                 intrpt, interrupt
mov                 ah,02Ch
int                 21h
mov                 pre_val, dh
mov                 timer, secs
mov                 sec,0
mov                 display_time, disp_time
jmp                 this

master_triggered:

popa
jmp                 origin



input               db ?

endd_with_input:

mov                 input, al
popa
mov                 al, input
jmp                 endd

this:

cmp             display_time,1
jne             here
outt2           timer,1,0245h,1
here:
cmp             sec, secs
je              endd_with_no_input
cmp             intrpt,01h
je              int_enabled
jmp             int_disabled

int_enabled:

pusha
mov             al,0
mov             inp,0
mov             bl,1
mov             int_enabled_macro,bl
call            master_chck
cmp             master_trigger,1
je              master_triggered
mov             al,inp
cmp             al,00
jne             endd_with_input
popa
mov             ah,2ch
int             21h
cmp             pre_val,dh
je              this
inc             sec
dec             timer
mov             pre_val,dh
jmp             this

int_disabled:


call            master
cmp             master_trigger,1
je              origin
mov             ah,2ch
int             21h
cmp             pre_val,dh
je              this
inc             sec
dec             timer
mov             pre_val,dh
jmp             this

endd_with_no_input:

cmp             intrpt,1
jne             endd
call            cls
print           late_msg
inc             late
endd:
endm

;****************************

outt macro                  outtt,positioning_stats,posi

local endd,status,no_posi,checking

pusha
mov                     al, positioning_stats
mov                     positioning_status,al

checking:

cmp                     positioning_status, 1
jne                     no_posi
call                    get_cursor_position
set_cursor_position     posi

no_posi:

mov                     al,outtt
mov                     ah, 0eh
int                     10H
cmp                     positioning_status, 1
jne                     endd
set_cursor_position     prev_position

endd:

popa

endm

;**************************

outt2 macro                 2digno,positioning_stats,posi,add48

local no_posi,here,checking,endd

pusha
mov                     addd,add48
mov                     al, positioning_stats
mov                     positioning_status,al

checking:

cmp                     positioning_status, 1
jne                     no_posi
call                    get_cursor_position
set_cursor_position     posi

no_posi:

mov                     al, 2digno
mov                     ah, 0
mov                     bl, 10
idiv                    bl
cmp                     addd,0
je                      here
add                     ah,48
add                     al,48

here:

lea                     si, twodigno
mov                     [si],al
inc                     si
mov                     [si],ah
print                   twodigno
cmp                     positioning_status, 1
jne                     endd
set_cursor_position     prev_position
endd:

popa

endm

;**********************

set_cursor_position macro position

pusha
mov                 bh,0
mov                 dx, position
mov                 ah,2
int                 10h
popa

endm

;**********************

print macro msg, duration

pusha
lea                 dx, msg
call                prnt
popa

endm

;*********************

sec_triggered_check macro

local here


call        sec_chk
cmp         master_trigger,1
je          origin
cmp         sec_triggered,0
je          here
call        reset_sec
jmp         origin

here:

endm

;*********************

;******************************************************************************
;******************************************************************************
;******************************************************************************

name " Home Security System"
org  100h

origin:

mov         al,0
mov         master_trigger,al
cmp         late,0
je          start
w8          5,0,0
mov         late,al

start:

call        cls
print       start_msg
input       1,0,0,0,1,0

enter_userno.:

call        cls
print       enter_user_no_msg
input       2,1,0,1,0,7
w8          1,0,0

checking:

lea         si, user_no
mov         cx,12

check:

mov     bl, [si]
cmp     bl,co_user[2]
je      pass
inc     si

loop check

inc         tiny
call        cls
print       wrong_user_no_msg
w8          7,0,0
sec_triggered_check
jmp         origin

pass:

lea         si, passes

off_add:;getting the offset address of the password

mov     bl, co_user[2]
dec     bl
mov     al,0

chck:

cmp     [si],0
je      inc_al
inc     si
jmp     chck

inc_al:

cmp     al, bl
je      out_off_add
inc     si
inc     al
jmp     chck

out_off_add:

mov     al,0
mov     pass_offset,si
inc     si

pass_dig_cntin:

cmp     [si],0
je      pasw_dig
inc     si
inc     al
jmp     pass_dig_cntin

pasw_dig:

mov     pass_dig, al
call    master_chck
cmp     master_trigger,1
je      origin
random  pass_dig

call    reset_sec
call    cls
print   correct_userno_msg
w8      3,0,0
call    cls


lea             si,ran
mov             dl, 1
cal_the_range   pass_dig
mov             dh,bh

user_pass_ent:

print       enter_dig_no_msg

call        get_cursor_position
mov         ax, prev_position
mov         pos_ran,ax

mov         al,[si]
mov         pass_no,al
outt2       pass_no,0,0,1

call        master_chck
cmp         master_trigger,1
je          origin
call        clrkeybuf
print       of_ur_pass_msg

call        get_cursor_position
mov         ax, prev_position
mov         user_pass_enter,ax

first_onward:

call        master_chck
cmp         master_trigger,1
je          master_triggered
mov         store_dh,dh
mov         store_dl,dl
mov         store_si,si
input       1,1,0,0,0,7


w8          1,0,0
set_cursor_position         user_pass_enter

outt        0,0,0; to clear the previous pass digit entered by the user

mov         ah, output
mov         bx, pass_offset
mov         cx,0
mov         cl,pass_no
add         bx, cx

cmp         ah,[bx]
jne         wrong

mov         dh,store_dh
mov         dl,store_dl
mov         si,store_si

cmp         dl, dh
je          open_up

inc         si
inc         dl
mov         al,[si]
mov         pass_no,al

set_cursor_position pos_ran

outt2       pass_no,0,0,1

set_cursor_position         user_pass_enter

jmp         first_onward

master_triggered:

jmp         origin

wrong:

call    cls
print   wrong_pass_msg
w8      4,0,0
sec_triggered_check
jmp     origin

open_up:

call    reset_sec
mov     al,0
mov     hound,al
mov     tiny,al
call    cls
print   accepted_msg
out     2,al
w8      30,0,0
mov     al,00000000b
out     2,al
jmp     origin

ret

;******************************************************************************
;******************************************************************************
;******************************************************************************
;***********************************Security Check*****************************
sec_chk proc

;input buffer should be cleared here, i guess...could increase the security by asking the digit no.
;that isn't in his pass say pass digit=14 ask him to enter 16, if entered screw him:P
;********************the procedure hasn't been used yet, STRONG DISPLAYS********

pusha
inc tiny
call            master_chck
cmp             master_trigger,1
je              start1
hound1:

cmp         hound,3
jne         tiny1
call        cls
print       hound_msg
mov         al,00001111b
out         1,al
w8          255,0,0
w8          255,0,0
w8          255,0,0
w8          135,0,0
mov         al,0
mov         hound,al
mov         tiny,al
mov         al,1
mov         sec_triggered,al
popa
ret

tiny1:

cmp         tiny, 5
jna         start1; needs to be defined
call        cls
print       tiny_msg
mov         al,00001100b
out         1,al
w8          180,0,0
mov         al, 0
mov         tiny,al
mov         al,1
mov         sec_triggered,al
inc         hound

start1:

popa
ret

sec_chk endp

;**************************

cls proc

pusha
mov             al,00
mov             bh,7
mov             cx,0
mov             dx, 24_79
mov             ah, 06
int             10h
mov             dx,0
mov             bh,0
mov             ah,2
int             10h
popa
ret

cls endp

;**************************

prnt proc near

pusha
mov         ah,9
int         21h
popa
ret

prnt endp

;**************************

clrkeybuf proc

pusha

zero:
mov     ah,6
mov     dl,255
int     21h
jne     zero
popa
ret

clrkeybuf endp

;**************************

reset_sec proc

pusha
mov         ax,0
mov         tiny,al
;mov         hound, al
mov         in_input_macro,0
mov         sec_triggered,0
popa
ret

reset_sec endp

;**************************

w8inp proc

pusha
mov         ah,00
int         16h
mov         inp,al
popa
mov         al,inp
ret

w8inp endp

;**************************

master proc

pusha
lea         si, master_pass
add         si, master_counter
cmp         al, [si]
je          here_counter_check

input_chck:
cmp         in_input_macro,1;when inside input macro in_input_macro=1
je          no_input
mov         ah, 6
mov         dl, 255
int         21h
je          no_input
mov         inp, al
cmp         al, [si]
jne         here_main

here_counter_check:

cmp         master_counter, 12
jne         here1_master


call        cls
print       master_msg
mov         al, 11111111b
out         2,  al
w8          10,0,0
mov         al,1
mov         master_trigger,al
mov         ax, 0
mov         master_counter,ax
mov         tiny,   al
mov         hound,  al
mov         in_input_macro,al
popa
ret
;rest of it remains

here1_master:

inc         master_counter
inc         si;add si, master_counter
cmp         int_enabled_macro,1
je          no_input
jmp         input_chck

here_main:

mov         ax, 0
mov         master_counter, ax
mov         in_input_macro,al
lea         si, master_pass
mov         al, inp
cmp         [si],al
je          input_chck
popa
ret

no_input:

mov         al,0
mov         in_input_macro,al
mov         int_enabled_macro,al
popa
ret

master endp

;**************************

master_chck proc

pusha
mov         al,0
call        master
popa

master_chck endp

;**************************

get_cursor_position proc

pusha
mov         bh,0
mov         ah,03
int         10h
mov         prev_position,dx
popa
ret

get_cursor_position endp

;**************************

late                db 0
store_dl            db ?
store_dh            db ?
store_si            dw ?
int_enabled_macro   db 0
master_trigger      db 0
sec_triggered       db 0
in_input_macro      db 0
user_pass_enter     dw ?
pos_ran             dw ?
input_char_counter  db ?
prev_position       dw ?
pass_no             db ?
pass_dig            db ?;**
user_no             db 1,2,3,4,5,6,7,8,9,10,11,12
inp                 db ?
hound               db 0
tiny                db 0
co_user             db 3 dup(?)
passes              db 0,'01234567890123',0,'234567',0,'2456578',0,'1775662',0,'173912',0,'8972247',0,'24689348',0,'6557892138206',0,'7821115576798',0,'474115618589',0,'34613367912',0,'9217472',0
pass_digs           db 9,6,7,7,6,7,8,13,13,12,11,7
ran                 db 9 dup(?);***
pass_offset         dw ?
master_pass         db '**123456789**'
master_counter      dw 0
pre_val             db ?
timer               db ?
sec                 db ?
intrpt              db ?
positioning_status  db ?
display_time        db ?
output              db ?
addd                db ?
twodigno            db 2 dup(?),'$'
start_msg           db 'PLEASE PRESS',96,'*',96,'$'
enter_user_no_msg   db 'PLEASE ENTER YOUR USER NUMBER, IN TWO DIGIT FORM(E.g: 02),WITHIN 7 SECONDS','$'
wrong_user_no_msg   db 'WRONG USER NO ENTERED','$'
late_msg            db 'Time out','$'
wrong_pass_msg      db 'ACCESS DENIED','$'
master_msg          db 'MASTER PASSWORD DETECTED','$'
tiny_msg            db 'UNDESIRED INPUT,THE SYSTEM WILL NOW HALT FOR 3 MINUTES','$'
hound_msg           db 'INTRUDER ALERT.THE SYSTM WILL SELF RESTART IN 15 MINUTES','$'
accepted_msg        db 'ACCESS GRANTED','$'
correct_userno_msg  db 'CORRECT USER # ENTERED','$'
enter_dig_no_msg    db 'ENTER DIGIT # ','$'
of_ur_pass_msg      db ' OF YOUR PASSWORD, WITHIN 7 SEC----','$'